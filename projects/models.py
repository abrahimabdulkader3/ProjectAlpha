from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Project(models.Model):
    name = models.CharField(
        max_length=200
    )  # Constraint of up to 200 characters long
    description = (
        models.TextField()
    )  # TextField allows for unlimited characters
    owner = models.ForeignKey(
        User, related_name="projects", on_delete=models.CASCADE, null=True
    )


def __str__(self):
    return self.name
