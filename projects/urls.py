from django.urls import path
from projects.views import list_project
from projects.views import project_details
from projects.views import create_project

urlpatterns = [
    path("", list_project, name="list_projects"),
    # "" -> represents the root of the file, show_project refers to the view function and the name is used for a reference
    path("<int:id>/", project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
