from django.shortcuts import get_object_or_404, render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def list_project(request):  # Method takes a request parameter
    show_projects = Project.objects.filter(
        owner=request.user
    )  # Retrieves all records from the Project model
    context = {
        "show_projects": show_projects  # Puts the projects in a dictionary
    }
    return render(
        request, "list_projects.html", context
    )  # Renders the html template


@login_required
def project_details(request, id):
    project = get_object_or_404(Project, id=id)
    return render(request, "project_detail.html", {"project": project})


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.save()
            return redirect("list_projects")

    else:
        form = ProjectForm()
    return render(request, "create_project.html", {"form": form})
