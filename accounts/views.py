from django.contrib.auth import logout
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from accounts.forms import LoginForm
from accounts.forms import SignupForm
from django.contrib.auth.models import User

# Create your views here.


def login_view(request):
    if (
        request.method == "POST"
    ):  # Checks if the request method is a POST (form submission)
        form = LoginForm(
            request.POST
        )  # Passes the data from the user for validation
        if (
            form.is_valid()
        ):  # If its valid, the data picks up on the username and password
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(
                request, username=username, password=password
            )  # authenticates the user

            if (
                user is not None
            ):  # If the user authentication is successful, redirect them to the login and redirect to list projects page
                login(request, user)
                return redirect("list_projects")
            else:  # Otherwise, throw an error to the page
                form.add_error(None, "Wrong username or password")

    else:  # Otherwise, get an instance of the LoginForm
        form = LoginForm()

    return render(
        request, "accounts/login.html", {"form": form}
    )  # render the login.html template


def logout_view(request):
    logout(request)
    return redirect("login")


# accounts/views.py


def signup_view(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = User.objects.create(username=username)
            user.set_password(password)
            user.save()
            login(request, user)
            return redirect("list_projects")

    else:
        form = SignupForm()

    return render(request, "accounts/signup.html", {"form": form})
