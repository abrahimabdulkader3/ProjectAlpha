from django.urls import path
from accounts.views import login_view
from accounts.views import logout_view
from accounts.views import signup_view

urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", signup_view, name="signup"),
]
