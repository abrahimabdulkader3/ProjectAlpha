from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


# Create your views here.
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.save()
            return redirect("list_projects")

    else:
        form = TaskForm()
    return render(request, "create_task.html", {"form": form})


@login_required
def my_tasks(request):
    assigned_tasks = Task.objects.filter(assignee=request.user)
    return render(request, "my_tasks.html", {"tasks": assigned_tasks})
